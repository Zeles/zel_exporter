FROM golang:1.13.0-buster

LABEL maintainer="Boris Platonov Zeles1000@gmail.com"

RUN apt update

RUN go get github.com/prometheus/client_golang/prometheus

WORKDIR /app

COPY . .

RUN go build -o rpi_exporter .

EXPOSE 7778

CMD ["./rpi_exporter"]