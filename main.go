package main

import (
	"io/ioutil"
	"log"
	"flag"
	"net/http"
	"bytes"
	"os/exec"
	"path/filepath"
	"regexp"
	"strconv"
	"fmt"
	"time"

	"github.com/prometheus/client_golang/prometheus"
	"github.com/prometheus/client_golang/prometheus/promhttp"
)

var addr = flag.String("listen-address", ":7778",
  "The address to listen on for HTTP requests.")

type Client struct {
  CpuTemp           prometheus.Gauge
  CpuVoltage        prometheus.Gauge
  CpuCurrentHertz   *prometheus.GaugeVec
  RamCVoltage       prometheus.Gauge
  RamIVoltage       prometheus.Gauge
  RamPVoltage       prometheus.Gauge
  LowVoltage        prometheus.Gauge
}

func cpuTemp(ct prometheus.Gauge) {
  for {
    cmd := "vcgencmd measure_temp | egrep -o '[0-9]*\\.[0-9]*'"
    command := exec.Command("bash", "-c", cmd)
    var out bytes.Buffer
    command.Stdout = &out
    command.Run()

    t, err := strconv.ParseFloat(out.String()[:len(out.String())-1], 64)
    if err != nil {
      fmt.Print(err.Error())
    }
    ct.Set(t)
    time.Sleep(1000 * time.Millisecond)
  }
}

func cpuVoltage(cv prometheus.Gauge) {
  for {
    cmd := "vcgencmd measure_volts | egrep -o '[0-9]*\\.[0-9]*'"
    command := exec.Command("bash", "-c", cmd)
    var out bytes.Buffer
    command.Stdout = &out
    command.Run()

    t, err := strconv.ParseFloat(out.String()[:len(out.String())-1], 64)
    if err != nil {
      fmt.Print(err.Error())
    }
    cv.Set(t)
    time.Sleep(1000 * time.Millisecond)
  }
}

func ramVoltageC(cv prometheus.Gauge) {
  for {
    cmd := "vcgencmd measure_volts sdram_c | egrep -o '[0-9]*\\.[0-9]*'"
    command := exec.Command("bash", "-c", cmd)
    var out bytes.Buffer
    command.Stdout = &out
    command.Run()

    t, err := strconv.ParseFloat(out.String()[:len(out.String())-1], 64)
    if err != nil {
      fmt.Print(err.Error())
    }
    cv.Set(t)
    time.Sleep(1000 * time.Millisecond)
  }
}

func ramVoltageI(cv prometheus.Gauge) {
  for {
    cmd := "vcgencmd measure_volts sdram_i | egrep -o '[0-9]*\\.[0-9]*'"
    command := exec.Command("bash", "-c", cmd)
    var out bytes.Buffer
    command.Stdout = &out
    command.Run()

    t, err := strconv.ParseFloat(out.String()[:len(out.String())-1], 64)
    if err != nil {
      fmt.Print(err.Error())
    }
    cv.Set(t)
    time.Sleep(1000 * time.Millisecond)
  }
}

func ramVoltageP(cv prometheus.Gauge) {
  for {
    cmd := "vcgencmd measure_volts sdram_p | egrep -o '[0-9]*\\.[0-9]*'"
    command := exec.Command("bash", "-c", cmd)
    var out bytes.Buffer
    command.Stdout = &out
    command.Run()

    t, err := strconv.ParseFloat(out.String()[:len(out.String())-1], 64)
    if err != nil {
      fmt.Print(err.Error())
    }
    cv.Set(t)
    time.Sleep(1000 * time.Millisecond)
  }
}

func getthrottled(cv prometheus.Gauge) {
  for {
    cmd := "vcgencmd get_throttled | egrep -o '0x([0-9a-fA-F]+)'"
    command := exec.Command("bash", "-c", cmd)
    var out bytes.Buffer
    command.Stdout = &out
    command.Run()

    t, err := strconv.ParseInt(out.String()[:len(out.String())-1], 0, 64)
    if err != nil {
      fmt.Print(err.Error())
    }
    if t & 1 == 0 {
      cv.Set(1)
    } else {
      cv.Set(0)
    }
    time.Sleep(1000 * time.Millisecond)
  }
}

func getCurrentCpuHertz(ch *prometheus.GaugeVec) {
  for {
    files, err := ioutil.ReadDir("/sys/devices/system/cpu")
    if err != nil {
      fmt.Print(err)
      fmt.Print("\n")
    }
    for _, f := range files {
      if f.IsDir() {
        regexp1 := regexp.MustCompile("cpu[0-9]+")
        dir := regexp1.FindString(f.Name())
        if dir != "" {
          path := filepath.Join("/", "sys", "devices", "system", "cpu", dir, "cpufreq", "scaling_cur_freq")
          file, err := ioutil.ReadFile(path)
          if err != nil {
            fmt.Print(err)
            fmt.Print("\n")
          }
          regexp2 := regexp.MustCompile("[0-9]+")
          number := regexp2.FindString(f.Name())
          if err != nil {
            fmt.Print(err)
            fmt.Print("\n")
          }
          hertz, err := strconv.ParseInt(string(file)[:len(file)-1], 0, 64)
          ch.WithLabelValues(number).Set(float64(hertz))
        }
      }
    }
    time.Sleep(1000 * time.Millisecond)
  }
}

func (c *Client)init() {
  c.CpuTemp = prometheus.NewGauge(prometheus.GaugeOpts{
    Name: "cpu_temperature",
    Help: "Current temperature of the CPU.",
  })
  c.CpuVoltage = prometheus.NewGauge(prometheus.GaugeOpts{
    Name: "cpu_voltage",
    Help: "Current voltage of the CPU.",
  })
  c.RamCVoltage = prometheus.NewGauge(prometheus.GaugeOpts{
    Name: "ram_c_voltage",
    Help: "Current voltage of the RAM C.",
  })
  c.RamIVoltage = prometheus.NewGauge(prometheus.GaugeOpts{
    Name: "ram_i_voltage",
    Help: "Current voltage of the RAM I.",
  })
  c.RamPVoltage = prometheus.NewGauge(prometheus.GaugeOpts{
    Name: "ram_p_voltage",
    Help: "Current voltage of the RAM P.",
  })
  c.LowVoltage = prometheus.NewGauge(prometheus.GaugeOpts{
    Name: "low_voltage",
    Help: "low voltage",
  })
  c.CpuCurrentHertz = prometheus.NewGaugeVec(
    prometheus.GaugeOpts{
      Name: "cpu_current_hertz",
    },
    []string{"id"},
  )
}

func main() {
  flag.Parse()
  client := new(Client)
  client.init()

  go cpuTemp(client.CpuTemp)
  go cpuVoltage(client.CpuVoltage)
  go ramVoltageC(client.RamCVoltage)
  go ramVoltageI(client.RamIVoltage)
  go ramVoltageP(client.RamPVoltage)
  go getthrottled(client.LowVoltage)
  go getCurrentCpuHertz(client.CpuCurrentHertz)

  prometheus.MustRegister(client.CpuTemp)
  prometheus.MustRegister(client.CpuVoltage)
  prometheus.MustRegister(client.RamCVoltage)
  prometheus.MustRegister(client.RamIVoltage)
  prometheus.MustRegister(client.RamPVoltage)
  prometheus.MustRegister(client.LowVoltage)
  prometheus.MustRegister(client.CpuCurrentHertz)

  http.Handle("/metrics", promhttp.Handler())

  log.Printf("Starting web server at %s\n", *addr)
  err := http.ListenAndServe(*addr, nil)
  if err != nil {
    log.Printf("http.ListenAndServer: %v\n", err)
  }
}